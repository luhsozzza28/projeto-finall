import { Card } from "./components/Card"
import { Footer } from "./components/Footer"
import { Header } from "./components/Header"
import resets from '../_resets.module.css';
import { BackgroundIcon } from './BackgroundIcon.js';
import classes from './MostrarSessoes.module.css';

function MostrarSessoes (){
  return(
    <div>
      <main>
        <Header />
        <div className={classes.horarios}>
          <div className={classes.group8}>
          <div className={classes.frame2}>
          <div className={classes._2d}>2d </div>
          </div>
          <div className={classes.frame3}>
          <div className={classes._3d}>3d</div>
         </div>
        <div className={classes.frame4}>
         <div className={classes.imax}>imax</div>
         </div>
          </div>
          <div className={classes.frame17}>
         <div className={classes.frame172}>
        <div className={classes.frame21}>
         <div className={classes.frame18}>
        <div className={classes.frame22}>
        <div className={classes._2d2}>2d </div>
        </div>
        <div className={classes.frame173}>
        <div className={classes.group13}>
        <div className={classes.frame42}>
        <div className={classes._1520}>15:20</div>
        </div>
        </div>
        <div className={classes.group14}>
          <div className={classes.frame43}>
          <div className={classes._1740}>17:40</div>
          </div>
          </div>
        <div className={classes.group15}>
        <div className={classes.frame44}>
        <div className={classes._200}>20:00</div>
        </div>
        </div>
        <div className={classes.group16}>
        <div className={classes.frame45}>
        <div className={classes._2210}>22:10</div>
        </div>
       </div>
        </div>
        </div>
       </div>
       </div>
       <div className={classes.frame182}>
         <div className={classes.frame212}>
        <div className={classes.frame183}>
        <div className={classes.frame23}>
        <div className={classes._3d2}>3d </div>
        </div>
        <div className={classes.frame174}>
         <div className={classes.group132}>
         <div className={classes.frame46}>
          <div className={classes._1530}>15:30</div>
           </div>
          </div>
        <div className={classes.group152}>
         <div className={classes.frame47}>
          <div className={classes._2015}>20:15</div>
          </div>
          </div>
            </div>
             </div>
              </div>
            </div>
            <div className={classes.frame213}>
              <div className={classes.frame184}>
            <div className={classes.frame24}>
            <div className={classes.imax2}>imax</div>
            </div>
           <div className={classes.frame175}>
            <div className={classes.group133}>
           <div className={classes.frame48}>
         <div className={classes._1620}>16:20</div>
      </div>
      </div>
    <div className={classes.group153}>
    <div className={classes.frame49}>
    <div className={classes._180}>18:00</div>
    </div>
      </div>
     </div>
      </div>
      </div>
          </div>
        </div>
        <div className={classes.fundo}></div>
        <div className={classes.frame1}>
          <div className={classes.bairro}>Bairro</div>
          <div className={classes.icRoundExpandLess}>
            <IcRoundExpandLessIcon className={classes.icon2} />
          </div>
        </div>
        <div className={classes.rectangle15}></div>
        <div className={classes.frame12}>
          <div className={classes.cidade}>Cidade</div>
          <div className={classes.icRoundExpandLess2}>
            <IcRoundExpandLessIcon2 className={classes.icon3} />
          </div>
        </div>
        <div className={classes.rectangle152}></div>
        <div className={classes.capa}></div>
        <div className={classes.rectangle17}></div>
        <div className={classes._12}>12</div>
        <div className={classes.quandoUmEscaravelhoAlienigenaS}>
          Quando um escaravelho alienígena se funde com seu corpo, Jaime ganha uma armadura tecnológica que lhe concede
          superpoderes incríveis.
        </div>
        <div className={classes.titulo}>Ação, Aventura</div>
        <div className={classes.besouroAzul}>Besouro Azul </div>
        <div className={classes.besouroAzul2}>Besouro Azul </div>
        <Header className={classes.header} />
      </main>
    </div>
    <Footer />
)
}
  

export default MostrarSessoes
