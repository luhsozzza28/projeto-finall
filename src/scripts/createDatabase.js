const sequelize = require("../config/database")

async function createDatabase() {
    try {
        await sequelize.sync();
        console.log("Criação com sucesso")
    } catch(error) {
        console.log(error.stack)
        console.log(error.name)
        console.log(error.message)
        console.log("houve erro na criação do banco de dados")
    }
}

module.exports = createDatabase;