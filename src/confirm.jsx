import "./global.css"
import resets from '../_resets.module.css';
import styles from './src.confirm.css';

function confirm () {
  return (
    <>
      <div className={classes.rectangle31}></div>
      <div className={classes.confirmacaoDeReserva}>Confirmação de Reserva!</div>
      <div className={classes.temCertezaDeQueDesejaConfirmar}>Tem certeza de que deseja confirmar a reserva?</div>
      <div className={classes.botao}>
        <div className={classes.confirmar}>confirmar</div>
      </div>
      <div className={classes.botao2}>
        <div className={classes.cancelar}>cancelar</div>
      </div>
    </>
  );
});

export default confirm
