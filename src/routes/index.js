const Router = require("express");

const userRoutes = require("./user.routes");
const movieRoutes = require("./movie.routes")

const router = Router();

router.use('/movie', movieRoutes)
router.use('/users', userRoutes);

module.exports = router;