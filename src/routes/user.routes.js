const express = require('express');

const userRoutes = express.Router();

//Inclusao dos middlewares
const checkUserExists = require("../middleware/checkUserExistsMiddleware");
const findUser = require("../middleware/findUserMiddleware");
//Inclusao dos controllers

const userController = require("../controllers/userController")

//Criar usuario
userRoutes.post('/registro', checkUserExists ,(request,response)=>userController.createUser(request,response))

//Login usuario
userRoutes.get("/login", (request,response)=>userController.userLogin(request,response))

module.exports = userRoutes;