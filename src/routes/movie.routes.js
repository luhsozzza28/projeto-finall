const express = require('express');

const movieRoutes = express.Router();

const movieController = require("../controllers/movieController")

movieRoutes.post('/' ,(req,res)=>movieController.createMovie(req,res))

module.exports = movieRoutes;
