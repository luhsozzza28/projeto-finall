const { User } = require("../models/models")
userController = require("../controllers/userController")
//Verifica se cpf e email ja estao presentes no db
async function checkUserExists(request,response, next){
    try {
        const { cpf, email } = request.body;
        const user = await User.findOne({
            where: {
                cpf
            }
        })
        if(user){
            return response.status(400).json({error: "CPF ja cadastrado!"})
        }
        try {
            const user = await User.findOne({
                where: {
                    email
                }
            })
            if(user){
                return response.status(400).json({error: "email ja cadastrado!"})
            }
            return next()
        }catch(error){
            console.log(error.name)
            console.log(error.stack)
            console.log(error.message)
            return response.status(400).json({error: "houve um erro ao verificar o email do usuario"})
        }
        
    }catch(error){
        return response.status(400).json({error: "houve um erro ao verificar o cpf do usuario"})
        
    }
}
module.exports = checkUserExists;
