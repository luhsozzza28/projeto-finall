import { Footer } from "./components/Footer"
import { Header } from "./components/Header"
import "./global.css"
import styles from "./register.module.css"

function Register() {
    return (
        <div>  
            <Header />
                <div>
                    <main className={styles.register}>
          
                        <div className={styles.call}>
                            <p>
                                Junte-se à Comunidade Cinematográfica!
                                Cadastre-se Aqui!
                            </p>
                        </div>
                        <div className={styles.calltext}>
                            <p>
                                Seja bem-vindo à nossa comunidade apaixonada pelo mundo do cinema. Ao fazer parte do nosso espaço digital, você está prestes a mergulhar em uma experiência cinematográfica única, onde a magia das telonas ganha vida com um toque moderno.
                            </p>
                            <p>
                                Nosso formulário de cadastro é o primeiro passo para embarcar nessa jornada emocionante. Ao preenchê-lo, você se tornará um membro da nossa comunidade, onde amantes do cinema se reúnem para compartilhar o entusiasmo, as emoções e as histórias que permeiam cada cena.
                            </p>
                        </div>
                        <form className={styles.regform}>
                            <h1>Registre-se</h1>     
                            <input type="text" name="nome" placeholder="Nome"/>
                            <input type="text" name="surname" placeholder="Sobrenome"/>
                            <input type="text" name="CPF" placeholder="CPF"/>
                            <input type="text" name="date" placeholder="Data de nascimento"/>
                            <input type="text" name="username" placeholder="Nome de Usuário"/>
                            <input type="email" name="email" placeholder="E-mail"/>
                            <input type="password" name="password" placeholder="Senha"/>
                            <input type="password" name="password" placeholder="Confirmar senha"/>
                            <button className={styles.regisbutton}>REGISTRAR</button>
                        </form>
                    </main>
                </div>
            <Footer/>
        </div>
    )
}
export default Register