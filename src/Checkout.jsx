const { Footer } = require("./components/Footer");
const { Header } = require("./components/Header");
import "./global.css"
import resets from '../_resets.module.css';
import { BackgroundIcon } from './BackgroundIcon.js';
import classes from './Checkout.module.css';
import { FundoIcon } from './FundoIcon.js';
import { TelaIcon } from './TelaIcon.js';

function Checkout() {
  return (
    <div className={`${resets.storybrainResets} ${classes.root}`}>
      <div className={classes.background}>
        <BackgroundIcon className={classes.icon} />
      </div>
      <Footer/>
      <div className={classes.fundo}></div>
      <div className={classes.legenda}>Legenda</div>
      <div className={classes.frame21}>
        <div className={classes.cadeira1}></div>
      </div>
      <div className={classes.disponivel}>Disponível</div>
      <div className={classes.cadeira12}></div>
      <div className={classes.comprado}>Comprado</div>
      <div className={classes.cadeira13}></div>
      <div className={classes.selecionado}>Selecionado</div>
      <div className={classes.line1}></div>
      <div className={classes.a}>a</div>
      <div className={classes.b}>b</div>
      <div className={classes.c}>C</div>
      <div className={classes.d}>D</div>
      <div className={classes.e}>e</div>
      <div className={classes.f}>f</div>
      <div className={classes.g}>g</div>
      <div className={classes.h}>h</div>
      <div className={classes.i}>i</div>
      <div className={classes.j}>j</div>
      <div className={classes.a2}>a</div>
      <div className={classes.b2}>B</div>
      <div className={classes.c2}>C</div>
      <div className={classes.d2}>D</div>
      <div className={classes.e2}>E</div>
      <div className={classes.f2}>f</div>
      <div className={classes.g2}>g</div>
      <div className={classes.h2}>h</div>
      <div className={classes.i2}>i</div>
      <div className={classes.j2}>j</div>
      <div className={classes.frame212}>
        <div className={classes.frame213}>
          <div className={classes.frame24}>
            <div className={classes.cadeira14}></div>
            <div className={classes.cadeira2}></div>
            <div className={classes.cadeira3}></div>
            <div className={classes.cadeira4}></div>
            <div className={classes.cadeira5}></div>
            <div className={classes.cadeira6}></div>
            <div className={classes.cadeira7}></div>
            <div className={classes.cadeira8}></div>
            <div className={classes.cadeira9}></div>
            <div className={classes.cadeira10}></div>
            <div className={classes.cadeira11}></div>
            <div className={classes.cadeira122}></div>
            <div className={classes.cadeira132}></div>
            <div className={classes.cadeira142}></div>
            <div className={classes.cadeira15}></div>
            <div className={classes.cadeira16}></div>
            <div className={classes.cadeira17}></div>
            <div className={classes.cadeira18}></div>
          </div>
        </div>
        <div className={classes.frame30}>
          <div className={classes.cadeira19}></div>
          <div className={classes.cadeira22}></div>
          <div className={classes.cadeira32}></div>
          <div className={classes.cadeira42}></div>
          <div className={classes.cadeira52}></div>
          <div className={classes.cadeira62}></div>
          <div className={classes.cadeira72}></div>
          <div className={classes.cadeira82}></div>
          <div className={classes.cadeira92}></div>
          <div className={classes.cadeira102}></div>
          <div className={classes.cadeira112}></div>
          <div className={classes.cadeira123}></div>
          <div className={classes.cadeira133}></div>
          <div className={classes.cadeira143}></div>
          <div className={classes.cadeira152}></div>
          <div className={classes.cadeira162}></div>
          <div className={classes.cadeira172}></div>
          <div className={classes.cadeira182}></div>
        </div>
        <div className={classes.frame31}>
          <div className={classes.cadeira110}></div>
          <div className={classes.cadeira23}></div>
          <div className={classes.cadeira33}></div>
          <div className={classes.cadeira43}></div>
          <div className={classes.cadeira53}></div>
          <div className={classes.cadeira63}></div>
          <div className={classes.cadeira73}></div>
          <div className={classes.cadeira83}></div>
          <div className={classes.cadeira93}></div>
          <div className={classes.cadeira103}></div>
          <div className={classes.cadeira113}></div>
          <div className={classes.cadeira124}></div>
          <div className={classes.cadeira134}></div>
          <div className={classes.cadeira144}></div>
          <div className={classes.cadeira153}></div>
          <div className={classes.cadeira163}></div>
          <div className={classes.cadeira173}></div>
          <div className={classes.cadeira183}></div>
        </div>
        <div className={classes.frame23}>
          <div className={classes.frame242}>
            <div className={classes.cadeira111}></div>
            <div className={classes.cadeira24}></div>
            <div className={classes.cadeira34}></div>
            <div className={classes.cadeira44}></div>
            <div className={classes.cadeira54}></div>
            <div className={classes.cadeira74}></div>
            <div className={classes.cadeira192}></div>
            <div className={classes.cadeira84}></div>
            <div className={classes.cadeira94}></div>
            <div className={classes.cadeira104}></div>
            <div className={classes.cadeira114}></div>
            <div className={classes.cadeira125}></div>
            <div className={classes.cadeira135}></div>
            <div className={classes.cadeira145}></div>
            <div className={classes.cadeira154}></div>
            <div className={classes.cadeira164}></div>
            <div className={classes.cadeira174}></div>
            <div className={classes.cadeira184}></div>
          </div>
        </div>
        <div className={classes.frame243}>
          <div className={classes.frame244}>
            <div className={classes.cadeira115}></div>
            <div className={classes.cadeira25}></div>
            <div className={classes.cadeira45}></div>
            <div className={classes.cadeira75}></div>
            <div className={classes.cadeira85}></div>
            <div className={classes.cadeira95}></div>
            <div className={classes.cadeira126}></div>
            <div className={classes.cadeira136}></div>
            <div className={classes.cadeira146}></div>
            <div className={classes.cadeira155}></div>
            <div className={classes.cadeira165}></div>
            <div className={classes.cadeira175}></div>
            <div className={classes.cadeira193}></div>
            <div className={classes.cadeira20}></div>
            <div className={classes.cadeira21}></div>
            <div className={classes.cadeira222}></div>
            <div className={classes.cadeira232}></div>
            <div className={classes.cadeira185}></div>
          </div>
        </div>
        <div className={classes.frame25}>
          <div className={classes.frame245}>
            <div className={classes.cadeira116}></div>
            <div className={classes.cadeira26}></div>
            <div className={classes.cadeira35}></div>
            <div className={classes.cadeira46}></div>
            <div className={classes.cadeira55}></div>
            <div className={classes.cadeira64}></div>
            <div className={classes.cadeira76}></div>
            <div className={classes.cadeira194}></div>
            <div className={classes.cadeira127}></div>
            <div className={classes.cadeira137}></div>
            <div className={classes.cadeira156}></div>
            <div className={classes.cadeira166}></div>
            <div className={classes.cadeira176}></div>
            <div className={classes.cadeira186}></div>
            <div className={classes.cadeira202}></div>
            <div className={classes.cadeira212}></div>
            <div className={classes.cadeira223}></div>
            <div className={classes.cadeira233}></div>
          </div>
        </div>
        <div className={classes.frame26}>
          <div className={classes.frame246}>
            <div className={classes.cadeira117}></div>
            <div className={classes.cadeira27}></div>
            <div className={classes.cadeira36}></div>
            <div className={classes.cadeira86}></div>
            <div className={classes.cadeira96}></div>
            <div className={classes.cadeira105}></div>
            <div className={classes.cadeira118}></div>
            <div className={classes.cadeira128}></div>
            <div className={classes.cadeira138}></div>
            <div className={classes.cadeira147}></div>
            <div className={classes.cadeira157}></div>
            <div className={classes.cadeira167}></div>
            <div className={classes.cadeira177}></div>
            <div className={classes.cadeira187}></div>
            <div className={classes.cadeira195}></div>
            <div className={classes.cadeira203}></div>
            <div className={classes.cadeira213}></div>
            <div className={classes.cadeira224}></div>
          </div>
        </div>
        <div className={classes.frame27}>
          <div className={classes.frame247}>
            <div className={classes.cadeira119}></div>
            <div className={classes.cadeira28}></div>
            <div className={classes.cadeira37}></div>
            <div className={classes.cadeira47}></div>
            <div className={classes.cadeira56}></div>
            <div className={classes.cadeira65}></div>
            <div className={classes.cadeira77}></div>
            <div className={classes.cadeira87}></div>
            <div className={classes.cadeira97}></div>
            <div className={classes.cadeira106}></div>
            <div className={classes.cadeira1110}></div>
            <div className={classes.cadeira148}></div>
            <div className={classes.cadeira158}></div>
            <div className={classes.cadeira168}></div>
            <div className={classes.cadeira178}></div>
            <div className={classes.cadeira188}></div>
            <div className={classes.cadeira196}></div>
            <div className={classes.cadeira204}></div>
          </div>
        </div>
        <div className={classes.frame28}>
          <div className={classes.frame248}>
            <div className={classes.cadeira120}></div>
            <div className={classes.cadeira29}></div>
            <div className={classes.cadeira38}></div>
            <div className={classes.cadeira48}></div>
            <div className={classes.cadeira57}></div>
            <div className={classes.cadeira66}></div>
            <div className={classes.cadeira78}></div>
            <div className={classes.cadeira88}></div>
            <div className={classes.cadeira98}></div>
            <div className={classes.cadeira1111}></div>
            <div className={classes.cadeira129}></div>
            <div className={classes.cadeira139}></div>
            <div className={classes.cadeira169}></div>
            <div className={classes.cadeira179}></div>
            <div className={classes.cadeira189}></div>
            <div className={classes.cadeira197}></div>
            <div className={classes.cadeira205}></div>
            <div className={classes.cadeira214}></div>
          </div>
        </div>
        <div className={classes.frame29}>
          <div className={classes.frame249}>
            <div className={classes.cadeira121}></div>
            <div className={classes.cadeira210}></div>
            <div className={classes.cadeira39}></div>
            <div className={classes.cadeira49}></div>
            <div className={classes.cadeira58}></div>
            <div className={classes.cadeira67}></div>
            <div className={classes.cadeira79}></div>
            <div className={classes.cadeira89}></div>
            <div className={classes.cadeira99}></div>
            <div className={classes.cadeira107}></div>
            <div className={classes.cadeira1112}></div>
            <div className={classes.cadeira1210}></div>
            <div className={classes.cadeira1310}></div>
            <div className={classes.cadeira149}></div>
            <div className={classes.cadeira159}></div>
            <div className={classes.cadeira1610}></div>
            <div className={classes.cadeira1710}></div>
            <div className={classes.cadeira1810}></div>
          </div>
        </div>
      </div>
      <div className={classes.luz}></div>
      <div className={classes.tela}>
        <TelaIcon className={classes.icon2} />
      </div>
      <div className={classes.tela2}>tela</div>
      <div className={classes.fundo2}>
        <FundoIcon className={classes.icon3} />
      </div>
      <div className={classes.botao}>
      <a href="./src/confirm.jsx" target="_blank">
        <div className={classes.confirmar}>confirmar</div></a>
      </div>
      <div className={classes.frame302}></div>
      <div className={classes.streamlineShoppingCatergoriesC}>
        <StreamlineShoppingCatergoriesC className={classes.icon4} />
      </div>
      <div className={classes.assentosEscolhidos}>Assentos escolhidos</div>
      <div className={classes.capa}></div>
      <div className={classes.besouroAzul}>Besouro Azul </div>
      <div className={classes.frame4}>
        <div className={classes._1520}>15:20</div>
      </div>
      <div className={classes.frame5}>
        <div className={classes._2D}>2D</div>
      </div>
      <Header />
    </div>
  );
});


export default Checkout