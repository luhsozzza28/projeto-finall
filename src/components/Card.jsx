import styles from "./card.module.css"

export function Card(props){
    return(
        <div className={styles.card}>
            <div className={styles.capa}>
                <img src="src\Imagens\Capa.png" alt="Capa do filme" />
            </div>
            <div>
                <div className={styles.titleline}>
                    <div className={styles.titulo}>
                        <p>{props.title}</p>
                    </div>
                    <div className={styles.idade}>
                        <img src="src/Assets/CLivre.svg"/>
                    </div>
                </div>
                <div className={styles.classinf}>
                    <p>{props.genre}</p>
                    <p>Direção: {props.direction} </p>
                    <p>{props.synopsis}</p>
                </div>
            </div>
            <div className={styles.info}>

            </div>
            <div className={styles.sessoesbutton}>
                <button>
                    Ver sessões
                </button>
            </div>
        </div>
    )
}