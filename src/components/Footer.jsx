import Styles from "./footer.module.css"

export function Footer() {
    return(
        <div>
            <div className={Styles.footer}>
                <div className={Styles.info}>
                    <div className={Styles.ender}>
                        <p>Endereço</p>
                        <address>   
                            Av. Milton Tavares de Souza, <br></br> s/n - Sala 115B - Boa Viagem, <br></br> Niterói - RJ <br></br>CEP:24210-315 
                        </address>
                    </div>
                    <div className={Styles.ctt}>
                        <p>Fale Conosco</p>
                        <address>
                            <a href="mailto:contato@injunior.com.br">contato@injunior.com.br</a>
                        </address>
                    </div>
                    <div className={Styles.cttbuttons}>
                        <button><img src="src/Assets/Icone-Instagram.svg" /></button>
                        <button><img src="src/Assets/Icone-Facebook.svg" /></button>
                        <button><img src="src/Assets/Icone-Linkedin.svg" /></button>
                    </div>
                </div>
                <div className={Styles.ticketimg}>
                    <img src="src/Imagens/INgresso.png" />
                </div>
                <div className={Styles.map}>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3090.484684340784!2d-43.1327444034781!3d-22.90501970305511!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1707145656686!5m2!1spt-BR!2sbr" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
            <div className={Styles.copyright}>
                <p>© Copyright 2023. IN Junior. Todos os direitos reservados. Niterói, Brasil.</p>
            </div>
        </div>
    )
}