import styles from "./header.module.css"

export function Header() {
    return(
        <div className={styles.header}>
            <div className={styles.logo}>
                <img src="src\Assets\Logo.svg"/>
            </div>
            <div className={styles.headerbuttons}>
                <button><img src="src\Assets\Icone-Filmes.svg"/></button>
                <button><img src="src\Assets\Icone-Entrar.svg"/></button>
                <button><img src="src\Assets\Icone-Ajuda.svg"/></button>
            </div>
        </div>
    )
}